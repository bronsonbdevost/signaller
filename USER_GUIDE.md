# User Guide

![demo](user-guide/signaller-demo.gif)

## Installation

Signaller is a PWA and can be run directly in the browser from the URL [https://signaller.brown-devost.com/](https://signaller.brown-devost.com/). It can also be installed as a desktop application using a browser supporting that functionality like Chrome, or on mobile devices with the add to home screen function.
![install image](user-guide/install.png)

## Getting Started with a Connection

The settings to connect to a SignalR hub can be found in the menu section, accessible via the "hamburger" icon at the top left corner of the window.
![connection menu button](user-guide/connection-menu-button.png)

This menu provides the ability to:

1. Set the SignalR Hub URL
1. Add a Javascript Web Token for autheentication
1. Initiate connection to the hub
1. Disconnect from the hub
1. Load a locally saved session
1. Save the current session locally

![connection menu](user-guide/connection-menu.png)

The parameters of the Javascript SignalR client can also be set from this pane by clicking on the gear icon to bring up the advanced connection menu.

![advanced connection settings button](user-guide/advanced-connection-menu-button.png)

The advanced connection menu makes it possible to configure all settings of the SignalR Javascript client. It provides the following options:

1. Selection of the SignalR client connector version
1. Selection of the HTTP transport type
1. Selection of logging level
1. Toggle on/off logging
1. Toggle connection negotiation (only available with WebSockets HTTP transport)

![advanced connection menu](user-guide/advanced-connection-menu.png)

Finally, all logging messages are displayed in the connection menu:

![connection logging](user-guide/connection-logging.png)

## Calling SignalR Hub Methods

Clicking on the "new" button in the main menu brings up a dialog where the name of the method can be entered.

**Note** that new methods will only be permanently added to the drop-down method list once they have been sent and returned a valid reponse. 

Individual paramaters for the method can be added by clicking the "+" button on the right side of the parameter list:

![add parameter button](user-guide/add-parameter-button.png)

Each parameter can be given a JSON type (string, number, bool, null, and JSON object). The value input field will adjust automatically according to the parameter type. For JSON parameter types a button will appear in the "parameter value" column; clicking this button will display a modal dialog with a JSON editor that provides minimal validation and formatting assistance. There is no "save" button in this JSON editor, clicking "exit" will save the current contents and undo can be used to step back any unwanted changes.

![edit json parameter](user-guide/json-edit.png)

Distinct names can be set for each method parameter. These are for reference only and do not have any effect on the method call (paramaters must be in correct order corresponding to the Hub method).

The Hub method is called by clicking on the "send" button, and a summary of the response will appear in the "Responses" pane below.

![method response](user-guide/method-response.png)

Clicking on the "info" icon for the response will bring up a modal dialog with the full response. Te response will be "pretty" printed, unless it is too large to be processed in a timely fashion.

![detailed method response](user-guide/method-detailed-response.png)

## Creating Listeners (local methods)

The gray listener panel on the left side of the window can be used to create listeners (or local methods) that a SignalR Hub can call. This is done by entering the name of the listener method in the "Listener Name" input field and clicking the "+" button next to it.

Once a listener has been added, it will appear in the list of listeners in the gray listener panel. Any listener can be deleted by clicking on the trash icon directly next to it.

![delete listener](user-guide/delete-listener.png)

Messages sent from the SignalR Hub to the listener will appear in the "Responses" panel with the name of the listener in the "Method" column.

![listener response](user-guide/listener-response.png)

## Info Footer

The footer of the app contains information about the current SignalR connection. The first icon (the "radar") will turn from red to green when a successful connection to the SignalR hub has been achieved. The next icon will be green if the connection is authenticated, otherwise it is red. Following these are the statistics about interactions with the hub: number of active listeners;  number of messages sent; number of messages successfully received; and number of errors received.

![status bar](user-guide/status-bar.png)