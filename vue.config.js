const MonacoEditorPlugin = require('monaco-editor-webpack-plugin')
var manifestJSON = require("./public/manifest.json");

// vue.config.js
module.exports = {
  configureWebpack: {
    plugins: [
      new MonacoEditorPlugin({languages: ['json']})
		]
	},
	pwa: {
    themeColor: manifestJSON.theme_color  
  }
}