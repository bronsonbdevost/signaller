declare module '*.vue' {
	import Vue from 'vue'
	export default Vue
}

declare module 'vue-monaco'
declare module 'vue-prism-component'
declare module 'vuedraggable'