import {
	HubConnectionBuilder as HubConnectionBuilder2,
	HubConnection as HubConnection2,
	ILogger as ILogger2,
	IHttpConnectionOptions as IHttpConnectionOptions2,
	LogLevel as LogLevel2,
	HttpTransportType as HttpTransportType2
} from '@aspnet/signalr'
import {
	HubConnectionBuilder as HubConnectionBuilder3,
	HubConnection as HubConnection3,
	ILogger as ILogger3,
	IHttpConnectionOptions as IHttpConnectionOptions3,
	LogLevel as LogLevel3,
	HttpTransportType as HttpTransportType3
} from '@microsoft/signalr'
import { v4 as uuid } from 'uuid'
import serializeError from 'serialize-error'
import { isEqual } from 'lodash'

export class SignalrClientConnection {
	public jwt: string
	public connectedJwt: string
	public url: string
	public connectedUrl: string
	private connection?: HubConnection2 | HubConnection3 = undefined
	private maxResponses: number

	public listeners: listenerObj[] = []
	public responses: responseObj[] = []
	public sentMessages: SentMessage[] = []
	public err?: string = undefined
	public loggedMessages: string[] = []
	public addMessage = (message: string) => this.loggedMessages.push(message)
	
	//SignalR connection settings
	private signalrVersion: SignalrVersion = SignalrVersion.v3
	private logLevel: LogLevel2 | LogLevel3 = this.signalrVersion === SignalrVersion.v2 ? LogLevel2.Debug : LogLevel3.Debug
	private loggingService: ILogger2 | ILogger3 = this.signalrVersion === SignalrVersion.v2 
		? new LoggingObject2(this.logLevel, this.addMessage)
		: new LoggingObject3(this.logLevel, this.addMessage)
	private signalConnectionsSettings: IHttpConnectionOptions2 | IHttpConnectionOptions3 = {
		logger: this.loggingService,
		logMessageContent: true,
		skipNegotiation: false,
		transport: this.signalrVersion === SignalrVersion.v2 ? HttpTransportType2.None : HttpTransportType3.None
	}

	public sentMessageCount: number = 0
	public receivedMessageCount: number = 0
	public errorCount: number = 0
	public listenerCount: number = 0

	constructor(url: string = '', maxResponses: number = 10, jwt: string = '') {
		this.url = url
		this.maxResponses = maxResponses
		this.jwt = jwt
		this.connectedJwt = ''
		this.connectedUrl = ''
	}

	/**
	 * Starts a SignalR Hub connection
	 * If a connection already exists, it will be stopped that a new one will be started
	 *
	 * @memberof signalrHub
	 */
	public async startConnection() {
		await this.stopConnection()
		try {
			if (this.jwt !== '') this.signalConnectionsSettings = {...this.signalConnectionsSettings, 
				accessTokenFactory: () => this.jwt!}

			let hubBuilder: HubConnectionBuilder2 | HubConnectionBuilder3 = this.signalrVersion === SignalrVersion.v2
				? new HubConnectionBuilder2()
				: new HubConnectionBuilder3().withAutomaticReconnect() // TODO: Check if this reloads listeners

			this.connection = hubBuilder.withUrl(this.url, this.signalConnectionsSettings)
																	.configureLogging(this.logLevel)
																	.build()

			if (this.connection !== undefined) {
				await this.connection.start()
				this.connection.onclose( async () => await this.stopConnection() )

				this.connectedUrl = this.url
				this.connectedJwt = this.jwt
			}
		} catch (err) {
			this.err = err
		}
	}

	/**
	 * Stops the current SignalR client connection if one exists
	 *
	 * @memberof signalrHub
	 */
	public async stopConnection() {
		this.err = undefined
		if (this.connection !== undefined) {
			await this.connection.stop()
			this.listeners = []
			this.loggedMessages = []
			this.connection = undefined
			this.connectedUrl = ''
			this.connectedJwt = ''
		}
	}

	/**
	 * Changes the settings for the SignalR Hub and restarts
	 *
	 * @param {SignalrVersion} signalrVersion An enum for the SignalR version to use (v2 or v3)
	 * @param {boolean} logMessageContent Whether to log message content
	 * @param {boolean} skipNegotiation Whether to skip negotiation (only works with websocket transport)
	 * @param {HttpTransportType2 | HttpTransportType3} transport Realtime transport to use
	 * @param {LogLevel2 | LogLevel3} logLevel Level for logging messages
	 * @memberof signalrHub
	 */
	public async setSignalrConfiguration(signalrVersion: SignalrVersion, logMessageContent: boolean, skipNegotiation: boolean,
		transport: HttpTransportType2 | HttpTransportType3, logLevel: LogLevel2 | LogLevel3, restart: boolean = true) {
		this.signalrVersion = signalrVersion
		this.logLevel = logLevel
		this.loggingService = this.signalrVersion === SignalrVersion.v2 
			? new LoggingObject2(this.logLevel, this.addMessage)
			: new LoggingObject3(this.logLevel, this.addMessage)
		this.signalConnectionsSettings = {
			logger: this.loggingService,
			logMessageContent: logMessageContent,
			skipNegotiation: skipNegotiation,
			transport: transport
		}
		if (restart)
			await this.startConnection()
	}

	/**
	 * Returns the current settings of the SignalR Hub
	 *
	 * @memberof signalrHub
	 */
	public getSignalrConfiguration(): SignalrSettingsObj {
		return new SignalrSettingsObj(this.signalrVersion, this.signalConnectionsSettings, this.logLevel)
	}

	/**
	 * Adds a new listener to the SignalR Hub
	 *
	 * @param {string} msg The new message method call the SignalR client will listen for
	 * @memberof signalrHub
	 */
	public addMessageListener(msg: string) {
		if (this.connection !== undefined) {
			const func = (resp: any) => {
				this.addJSONResponse(msg, resp)
				this.receivedMessageCount++
			}
			this.listeners.push(new listenerObj(msg, func))
			this.connection.on(msg, func)
			this.listenerCount++
		}
	}

	/**
	 * Removes an existing listener from the SignalR Hub
	 *
	 * @param {string} msg The name of the message method call to stop listening for
	 * @memberof signalrHub
	 */
	public removeMessageListener(msg: string) {
		if (this.connection !== undefined) {
			const filteredListeners = this.listeners.filter(x => x.msg === msg)
			for (const listener of filteredListeners) {
				this.connection.off(listener.msg, listener.func)
				this.listenerCount--
			}
			this.listeners = this.listeners.filter(x => x.msg !== msg)
		}
	}

	/**
	 * Sends a message to the SignalR Hub with the submitted parameters
	 *
	 * @param {string} msg The name of the message method on the SignalR Hub
	 * @param {...any[]} params The parameters to submit for the method
	 * @memberof signalrHub
	 */
	public async sendMessage(msg: string, ...params: Parameter[]): Promise<boolean> {
		return new Promise(async (resolve, reject) => {
			if (this.connection === undefined) reject(false)
			this.sentMessageCount++
			try {
				let resp = await this.connection!.invoke(msg, ...params.map(x => x.paramValue))
				this.addJSONResponse(msg, resp)
				this.receivedMessageCount++
				params = params.map(
					x => new Parameter(x.paramName, x.paramType, x.paramType === 'json' ? 
						JSON.stringify(x.paramValue) : x.paramValue))
				this.addSentMessage(new SentMessage(msg, params))
				resolve(true)
			} catch (err) {
				this.errorCount++
				this.addJSONResponse(msg, serializeError(err))
				reject(false)
			}
		})
	}

	/**
	 * Adds a message to the array of sent messages
	 *
	 * @param {SentMessage} newMessage The sent message to store in the array of sent messages
	 * @memberof signalrHub
	 */
	public async addSentMessage(newMessage: SentMessage) {
		for (let sentMesage of this.sentMessages) {
			if (sentMesage.msg === newMessage.msg && 
				isEqual(
					sentMesage.params.map(x => new ParameterDefinition(x.paramName, x.paramType)), 
					newMessage.params.map(x => new ParameterDefinition(x.paramName, x.paramType)))) {
				sentMesage.params = newMessage.params
				return
			}
		}
		this.sentMessages.push(newMessage)
	}

	/**
	 * Deletes a message to the array of sent messages
	 *
	 * @param {SentMessage} newMessage The sent message to delete from the array of sent messages
	 * @memberof signalrHub
	 */
	public async deleteSentMessage(deleteMessage: SentMessage) {
		for (let [idx, sentMesage] of this.sentMessages.entries()) {
			if (isEqual(sentMesage, deleteMessage)) {
				this.sentMessages.splice(idx, 1)
				return
			}
		}
	}

	/**
	 * Adds the response from a message listeneras a JSON object to the queue of responses
	 * Clears out old responses when maxResponses has been exceeded
	 *
	 * @private
	 * @param {string} msg The name of the message method that received notification
	 * @param {*} resp The notification that was sent from the SignalR Hub, which will be read as JSON
	 * @memberof signalrHub
	 */
	private addJSONResponse(msg: string, resp: any) {
		if (this.responses.length > this.maxResponses - 1) this.responses.shift()
		this.responses.push(new responseObj(msg, JSON.stringify(resp, null, 2)))
	}

	/**
	 * Returns an object representation of the current state of the service
	 * This can be stored as a file and later loaded back into the service
	 *
	 * @returns {FileStoreObj}
	 * @memberof SignalrClientConnection
	 */
	public getFileStoreObject(): FileStoreObj {
		return new FileStoreObj(this.url, this.maxResponses, this.listeners.map(x => x.msg), this.sentMessages, 
			this.getSignalrConfiguration(), this.jwt)
	}

	/**
	 * Loads settings from a fileStoreObj (probably saved as a file)
	 * Starts the connection with the loaded settings
	 *
	 * @memberof SignalrClientConnection
	 */
	public async loadFileStoreObject(fileStoreObj: FileStoreObj) {
		this.url = fileStoreObj.url
		this.maxResponses = fileStoreObj.maxResponses
		this.sentMessages = fileStoreObj.sentMessages.map(
			x => new SentMessage(
				x.msg, 
				x.params.map(x => new Parameter(
					x.paramName, 
					x.paramType, 
					x.paramType === 'json' ? 
						x.paramValue.replace(/^"/, '').replace(/(?:\\(.))/g, '$1').replace(/"$/, '') : 
						x.paramValue))))
		this.setSignalrConfiguration(
			fileStoreObj.settings.signalrVersion, 
			fileStoreObj.settings.connectionSettings.logMessageContent!, 
			fileStoreObj.settings.connectionSettings.skipNegotiation!,
			fileStoreObj.settings.connectionSettings.transport as HttpTransportType2 | HttpTransportType3, 
			fileStoreObj.settings.logLevel)
		this.jwt = fileStoreObj.jwt

		// Remover any currently running listeners
		for (const listener of this.listeners) this.removeMessageListener(listener.msg)

		// (Re)start the connection
		await this.startConnection()

		// Load the new listeners
		for (const listener of fileStoreObj.listeners) this.addMessageListener(listener)
	}
}

class listenerObj {
	public msg: string
	public func: any
	constructor(msg: string, func: any) {
		this.msg = msg
		this.func = func
	}
}

export class ParameterDefinition {
	public paramName: string
	public paramType: string

	constructor(paramName: string, paramType: string) {
		this.paramName = paramName
		this.paramType = paramType
	}
}

export class Parameter extends ParameterDefinition{
	public paramValue: any

	constructor(paramName: string, paramType: string, paramValue: any) {
		super(paramName, paramType)
		this.paramValue = paramValue
	}
}

export class SentMessage {
	public msg: string
	public params: Parameter[]

	constructor(msg: string, params: Parameter[]) {
		this.msg = msg
		this.params = params
	}
}

class responseObj {
	public id: string
	public time: Date
	public msg: string
	public response: string
	constructor(msg: string, response: string) {
		this.msg = msg
		this.response = response
		this.time = new Date()
		this.id = uuid()
	}
}

class SignalrSettingsObj {
	public signalrVersion: SignalrVersion
	public connectionSettings: IHttpConnectionOptions2 | IHttpConnectionOptions3
	public logLevel: LogLevel2 | LogLevel3

	constructor(signalrVersion: SignalrVersion, connectionSettings: IHttpConnectionOptions2 | IHttpConnectionOptions3,
		logLevel: LogLevel2 | LogLevel3){
			this.signalrVersion = signalrVersion
			this.connectionSettings = connectionSettings
			this.logLevel = logLevel
	}
}

export class FileStoreObj {
	public url: string
	public maxResponses: number
	public listeners: string[]
	public sentMessages: SentMessage[]
	public settings: SignalrSettingsObj
	public jwt: string

	constructor (url: string, maxResponses: number, listeners: string[], sentMessages: SentMessage[], 
		settings: SignalrSettingsObj, jwt: string = '') {
		this.url = url
		this.maxResponses = maxResponses
		this.listeners = listeners
		this.sentMessages = sentMessages
		this.settings = settings
		this.jwt = jwt
	}
}

class LoggingObject2 implements ILogger2 {
	private logLevel: LogLevel2
	private addMessage: (s: string) => void

	constructor(logLevel: LogLevel2, addMessage: (s: string) => void) {
		this.logLevel = logLevel
		this.addMessage = addMessage
	}

	public log(ll: LogLevel2, message: string) {
		if (ll >= this.logLevel) this.addMessage(message)
	}
}

class LoggingObject3 implements ILogger3 {
	private logLevel: LogLevel3
	private addMessage: (s: string) => void

	constructor(logLevel: LogLevel2, addMessage: (s: string) => void) {
		this.logLevel = logLevel
		this.addMessage = addMessage
	}

	public log(ll: LogLevel3, message: string) {
		if (ll >= this.logLevel) this.addMessage(message)
	}
}

export enum SignalrVersion {
	v2 = 'v2',
	v3 = 'v3'
}

export default SignalrClientConnection
