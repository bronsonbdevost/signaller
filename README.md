# signaller
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitlab.com/bronsonbdevost/signaller/blob/master/LICENSE)
[![FOSSA Status](https://app.fossa.io/api/projects/custom%2B12474%2Fgit%40gitlab.com%3Abronsonbdevost%2Fsignaller.git.svg?type=shield)](https://app.fossa.io/projects/custom%2B12474%2Fgit%40gitlab.com%3Abronsonbdevost%2Fsignaller.git?ref=badge_shield)

This is a handy Vue.js app to wrap the functionality of Microsoft's SignalR javascript client.  It runs as a PWA and can be installed as an app with Chrome.  It can be used to connect to a SignalR hub, call methods directly on the Hub, and create listeners to respond to server pushed events. Pull requests are very welcome!

If you just want to use the app, navigate to [signaller.brown-devost.com](https://signaller.brown-devost.com); see also the [Users Guide](USER_GUIDE.md) for instuctions.

![demo](user-guide/signaller-demo.gif)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
